# Minimal working example

This MWE demonstrates the API as discussed with Peter on the 9th of April. 

It solves issues:
- [Issue #5: preloading audio](https://gitlab.com/computational-audition-lab/psynet/-/issues/5)
- [Issue #6 on the front end](https://gitlab.com/computational-audition-lab/psynet/-/issues/6) and
- [Issue #8: adding a process bar to the preloading of the audio](https://gitlab.com/computational-audition-lab/psynet/-/issues/8)

## How to install
Just clone the repo and cd into the folder

Now run
```
python3 -m http.server 9000
```

You can view the site here: http://0.0.0.0:9000/

## Concatenating files
To concatenate files use `concat.py`.

Example usage: 
```
python concat.py file1.mp3 file2.mp3 file3.mp3 > concatenated_file.mp3
```

## Credits
Code is based on the post [Loading sound files faster using Array Buffers and Web Audio API](http://local-clicktorelease.com/blog/loading-sounds-faster-using-html5-web-audio-api)
